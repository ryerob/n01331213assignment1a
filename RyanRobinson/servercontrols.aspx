﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrols.aspx.cs" Inherits="RyanRobinson.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dinner Reservation</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Dinner Reservation</h1>
            <div>
            <asp:Label runat="server">Name: </asp:Label> 
            <asp:TextBox runat="server" ID="dinerName" placeholder="e.g. John Doe"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your name." ControlToValidate="dinerName" ID="validatorName"></asp:RequiredFieldValidator>
            </div>
            <div>
            <asp:Label runat="server">Phone: </asp:Label>
            <asp:TextBox runat="server" ID="dinerPhone" placeholder="e.g. 9052335555"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your phone number." ControlToValidate="dinerPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="dinerPhone" Type="String" Operator="NotEqual" ValueToCompare="9052337234" ErrorMessage="This is our phone number."></asp:CompareValidator>
            
            <%-- ValidationExpression used for phone number found at https://stackoverflow.com/questions/18585613/how-do-you-validate-phone-number-in-asp-net--%> 
           
            <asp:RegularExpressionValidator ID="phoneValid" runat="server" ErrorMessage="Please enter a valid phone number."  ControlToValidate="dinerPhone" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ></asp:RegularExpressionValidator>
            </div>
            <div>
            <asp:Label runat="server">E-mail: </asp:Label>
            <asp:TextBox runat="server" ID="dinerEmail" placeholder="e.g. johndoe@johnweb.net"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your e-mail address." ControlToValidate="dinerEmail" ID="validatorEmail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="emailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="dinerEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            </div>
            <div>
            <asp:Label runat="server">Time (P.M.): </asp:Label>
            <asp:DropDownList runat="server" ID="time">
                <asp:ListItem Value="5" Text="5:00"></asp:ListItem>
                <asp:ListItem Value="5.5" Text="5:30"></asp:ListItem>
                <asp:ListItem Value="6" Text="6:00"></asp:ListItem>
                <asp:ListItem Value="6.5" Text="6:30"></asp:ListItem>
                <asp:ListItem Value="7" Text="7:00"></asp:ListItem>
                <asp:ListItem Value="7.5" Text="7:30"></asp:ListItem>
                <asp:ListItem Value="8" Text="8:00"></asp:ListItem>
                <asp:ListItem Value="8.5" Text="8:30"></asp:ListItem>
                <asp:ListItem Value="9" Text="9:00"></asp:ListItem>
                <asp:ListItem Value="9.5" Text="9:30"></asp:ListItem>
                <asp:ListItem Value="10" Text="10:00"></asp:ListItem>
                <asp:ListItem Value="10.5" Text="10:30"></asp:ListItem>
                <asp:ListItem Value="11" Text="11:00"></asp:ListItem>
                <asp:ListItem Value="11.5" Text="11:30"></asp:ListItem>
            </asp:DropDownList>
            </div>
            <div>
            <asp:Label runat="server">Table Location: </asp:Label>
            <asp:RadioButton runat="server" Text="Restraunt" GroupName="via" checked/>
            <asp:RadioButton runat="server" Text="Patio" GroupName="via"/>
            </div>
            <div>
            <asp:Label runat="server">Complimentary Bread: </asp:Label>
            <asp:CheckBox runat="server" ID="WB" Text="White Bread" />
            <asp:CheckBox runat="server" ID="WGB" Text="Whole Grain Bread" />
            <asp:CheckBox runat="server" ID="GFB" Text="Gluten Free Bread" />
            </div>
            <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" DisplayMode="list"/>
            </div>
            <div>
            <asp:Button runat="server" ID="submitButton" Text="Submit" />
            </div>
        </div>

    </form>
</body>
</html>
